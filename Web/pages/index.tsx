// import Link from "next/link";

import { useEffect, useState, useCallback } from "react";
import { Theme } from "@mui/material/styles";

import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import FormControl from "@mui/material/FormControl";
import TextField from "@mui/material/TextField";

import makeStyles from "@mui/styles/makeStyles";

import DashboardLayout from "../src/components/Layout/DashboardLayout";
// import { MeConsumer } from "../context/MeContext";
import generateMetaTitle from "../lib/generateMetaTitle";
import RecordNotFound from "../src/components/RecordNotFound";
import { debounce } from "lodash";
import ProductCard from "../src/components/Card/ProductCard";

const useStyles = makeStyles((theme: Theme) => ({
  pageTitle: {
    fontSize: theme.typography.pxToRem(20),
    color: theme.palette.grey[600],
    fontWeight: "normal",
    marginTop: -20,
    marginBottom: 0,
  },
  searchLabel: {
    fontWeight: theme.typography.fontWeightBold,
    paddingRight: 10,
  },
  sortLabel: {
    fontWeight: theme.typography.fontWeightBold,
    paddingLeft: 20,
  },
  sort: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
  },
  divider: {
    borderColor: theme.palette.grey[300],
    margin: theme.spacing(4, 0, 7, 0),
  },
  grid: {
    [theme.breakpoints.down("sm")]: {
      padding: theme.spacing(2),
      "& > .MuiGrid-item": {
        width: "100%",
      },
    },
  },

  error: {
    width: "50%",
    margin: "0 auto",
  },
}));

const IndexPage = () => {
  const classes = useStyles();

  const [sort, setSort] = useState(0);
  const [search, setSearch] = useState("");
  const [items, setItems] = useState<any>([]);

  const handleSearchChange = (event: any) => {
    setSearch(event.target.value);
  };

  const handleSortChange = (event: any) => {
    setSort(parseInt(event.target.value));
  };

  const debounceSearchFn = useCallback(debounce(handleSearchChange, 800), []);

  useEffect(() => {
    let query = [];

    if (search) {
      query.push(`search=${search}`);
    }
    if (sort) {
      query.push(`sort=${sort}`);
    }

    const queryString = query.length > 0 ? "?" + query.join("&") : "";

    fetch(process.env.NEXT_PUBLIC_API_URL + "/products" + queryString, {
      method: "GET",
      mode: "cors",
      credentials: "same-origin",
      headers: {
        "Content-Type": "application/json",
      },
      referrerPolicy: "no-referrer",
    })
      .then((response) => response.json())
      .then((data) => setItems(data));
  }, [sort, search]);

  return (
    <div>
      <Typography align="center" variant={"h2"} className={classes.pageTitle}>
        Welcome to the Assessment's Shop
      </Typography>
      <div className={classes.sort}>
        <Typography
          align="right"
          variant={"body1"}
          className={classes.searchLabel}
        >
          Search By:
        </Typography>
        <TextField
          id="search"
          label=""
          variant="outlined"
          onChange={debounceSearchFn}
          placeholder="Search here..."
        />
        <Typography
          align="right"
          variant={"body1"}
          className={classes.sortLabel}
        >
          Sort By:
        </Typography>
        <FormControl
          sx={{ m: 1, minWidth: 120 }}
          variant="filled"
          size="small"
          hiddenLabel
        >
          <Select
            labelId="sort"
            id="sort"
            value={sort + ""}
            label="Sort"
            onChange={handleSortChange}
          >
            <MenuItem value={0}>
              <em>None</em>
            </MenuItem>
            <Divider />
            <MenuItem value={1}>
              <b>Product Name:</b>&nbsp;&nbsp;A to Z
            </MenuItem>
            <MenuItem value={2}>
              <b>Product Name:</b>&nbsp;&nbsp;Z to A
            </MenuItem>
            <Divider />
            <MenuItem value={3}>
              <b>Brand Name:</b>&nbsp;&nbsp;A to Z
            </MenuItem>
            <MenuItem value={4}>
              <b>Brand Name:</b>&nbsp;&nbsp;Z to A
            </MenuItem>
          </Select>
        </FormControl>
      </div>
      <Divider className={classes.divider} />
      {items && items.length > 0 ? (
        <Grid container spacing={2}>
          {items.map((item: any, index: number) => (
            <Grid item xs={12} sm={6} md={4} lg={3} key={index}>
              <ProductCard item={item} isEditAble />
            </Grid>
          ))}
        </Grid>
      ) : (
        <div className={classes.error}>
          <RecordNotFound
            title={
              "Opps! There is no products currently, kindly create a product now."
            }
            customDisplay
          />
        </div>
      )}
    </div>
  );
};

IndexPage.Layout = DashboardLayout;
IndexPage.layoutProps = { title: "Products", textAlign: "center" };
IndexPage.metaProps = { title: generateMetaTitle("Products") };

export default IndexPage;
