import { useEffect, useState } from "react";
import { useRouter } from "next/router";

import { Theme } from "@mui/material/styles";

import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";
import Paper from "@mui/material/Paper";
import TextField from "@mui/material/TextField";

import { makeStyles } from "@mui/styles";

import DashboardLayout from "../../../src/components/Layout/DashboardLayout";
import generateMetaTitle from "../../../lib/generateMetaTitle";
import ProductCard from "../../../src/components/Card/ProductCard";

type ErrorValue = {
  brand: boolean;
  productName: boolean;
  UPC12: boolean;
  image: boolean;
};

type ErrorKeys = keyof ErrorValue;

type FormValue = {
  brand: string;
  productName: string;
  UPC12: number;
  image?: string;
};

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    maxWidth: "1000px",
    margin: "0 auto",
  },
  paper: {
    padding: "50px",
    paddingLeft: theme.spacing(5),
    paddingRight: theme.spacing(5),
  },
  field: {
    width: "70%",
    [theme.breakpoints.down("lg")]: {
      width: "100%",
    },
  },
}));

//

const EditProductPage = () => {
  const classes = useStyles();

  const router = useRouter();
  const { id } = router.query;

  const [initProduct, setInitProduct] = useState<any>(null);
  const [formValue, setFormValue] = useState<FormValue>({
    brand: "",
    productName: "",
    UPC12: 0,
  });
  const [errorValue, setErrorValue] = useState<ErrorValue>({
    brand: false,
    productName: false,
    UPC12: false,
    image: false,
  });

  const onInput = (e: any) => {
    e.target.value = Math.max(0, parseInt(e.target.value))
      .toString()
      .slice(0, 12);
  };

  const onChange = (value: any, fieldName: string, regex?: RegExp) => {
    if (regex) {
      const reg = new RegExp(regex);

      setErrorValue({
        ...errorValue,
        [fieldName]: !reg.test(value + "") ? true : false,
      });
    }

    setFormValue({
      ...formValue,
      [fieldName]: value,
      // [fieldName]: !value && isEmptyDefault ? initProduct[fieldName] : value,
    });
  };

  const onReset = () => {
    setFormValue(initProduct);
    setErrorValue({
      brand: false,
      productName: false,
      UPC12: false,
      image: false,
    });
  };

  const onSubmit = () => {
    let isError = false;

    Object.keys(errorValue).forEach((key) => {
      if (errorValue[key as ErrorKeys]) {
        isError == true;
      }
    });

    if (id && formValue) {
      fetch(process.env.NEXT_PUBLIC_API_URL + "/products/" + id, {
        method: "PUT",
        mode: "cors",
        credentials: "same-origin",
        headers: {
          "Content-Type": "application/json",
        },
        referrerPolicy: "no-referrer",
        body: JSON.stringify(formValue),
      })
        .then((response) => {
          if (response.status != 200) {
            throw new Error(response.statusText);
          }
          return response.json();
        })
        .then((data) => {
          alert(data.message);
          router.push("/");
        })
        .catch((err) => alert(err.message));
    } else {
      alert("Cant submit when no product ID or form data");
    }
  };

  useEffect(() => {
    if (id) {
      fetch(process.env.NEXT_PUBLIC_API_URL + "/products/" + id, {
        method: "GET",
        mode: "cors",
        credentials: "same-origin",
        headers: {
          "Content-Type": "application/json",
        },
        referrerPolicy: "no-referrer",
      })
        .then((response) => response.json())
        .then((data) => {
          setInitProduct(data);
          setFormValue(data);
        });
    }
  }, [id]);

  if (id && formValue) {
    return (
      <Grid container spacing={2}>
        <Grid item xs={12} sm={6} md={4} lg={3}>
          <ProductCard item={formValue} />
        </Grid>
        <Grid item xs={12} sm={6} md={8} lg={9}>
          <Paper className={classes.paper}>
            <form>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <TextField
                    id="productName"
                    value={formValue.productName}
                    label="Product Name"
                    placeholder="Product Name"
                    variant="outlined"
                    required
                    className={classes.field}
                    onChange={(e) =>
                      onChange(
                        e.target.value,
                        "productName",
                        /(.|\s)*\S(.|\s)*/g
                      )
                    }
                    error={errorValue.productName}
                    helperText={errorValue.productName && "Cant be empty"}
                    InputLabelProps={{ shrink: true }}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    id="brand"
                    value={formValue.brand}
                    label="Brand Name"
                    placeholder="Brand Name"
                    variant="outlined"
                    required
                    className={classes.field}
                    onChange={(e) =>
                      onChange(e.target.value, "brand", /(.|\s)*\S(.|\s)*/g)
                    }
                    error={errorValue.brand}
                    helperText={errorValue.brand && "Cant be empty"}
                    InputLabelProps={{ shrink: true }}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    id="UPC12"
                    value={formValue.UPC12}
                    label="UPC12 Barcode"
                    variant="outlined"
                    type={"number"}
                    onInput={onInput}
                    className={classes.field}
                    onChange={(e) =>
                      onChange(parseInt(e.target.value), "UPC12", /^\d{12}$/g)
                    }
                    required
                    error={errorValue.UPC12}
                    helperText={
                      errorValue.UPC12 &&
                      "Cant be empty and must be 12 digit of number"
                    }
                    InputLabelProps={{ shrink: true }}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    id="image"
                    placeholder="https://example.com/image.png"
                    value={formValue.image}
                    label="Image Link / URL"
                    variant="outlined"
                    className={classes.field}
                    onChange={(e) =>
                      onChange(
                        e.target.value,
                        "image",
                        /^((?:https?:\/\/)?(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b[-a-zA-Z0-9@:%_\+.~#?&\/=]*|)$/g
                      )
                    }
                    error={errorValue.image}
                    helperText={
                      errorValue.image && "Must be a valid image link"
                    }
                    InputLabelProps={{ shrink: true }}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Button variant="contained" onClick={onSubmit}>
                    Submit Now
                  </Button>
                </Grid>
                <Grid item xs={12}>
                  <Button variant="outlined" onClick={onReset}>
                    Reset
                  </Button>
                </Grid>
              </Grid>
            </form>
          </Paper>
        </Grid>
      </Grid>
    );
  } else {
    return;
  }
};

EditProductPage.Layout = DashboardLayout;
EditProductPage.layoutProps = {
  title: "Edit Product",
  textAlign: "center",
};
EditProductPage.metaProps = { title: generateMetaTitle("Product") };

export default EditProductPage;
