/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  productionBrowserSourceMaps: true,
  staticPageGenerationTimeout: 200,
  swcMinify: true,
};

module.exports = nextConfig;
