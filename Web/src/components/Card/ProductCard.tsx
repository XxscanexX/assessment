import clsx from "clsx";
import * as React from "react";
import { Theme } from "@mui/material/styles";

import Button from "@mui/material/Button";
import Divider from "@mui/material/Divider";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";

import makeStyles from "@mui/styles/makeStyles";
import MyLink from "../MyLink";
import Barcode from "react-barcode";

interface Props extends React.PropsWithChildren {
  item: any;
  isEditAble?: boolean;
}

const useStyles = makeStyles((theme: Theme) => ({
  card: {
    borderRadius: 10,
    "&:hover": {
      boxShadow: "0px 7px 20px rgba(56, 94, 217,.3)",
    },
  },
  cardContainer: {
    padding: 0,
  },
  image: {
    objectFit: "cover",
    width: "100%",
    height: "30vh",
    borderBottom: "1px solid rgba(0,0,0,.1)",
    [theme.breakpoints.down("sm")]: {
      height: "25vh",
    },
  },
  word: {
    textDecoration: "none",
  },
  content: {
    padding: theme.spacing(2, 3),
  },
  title: {
    fontSize: theme.typography.pxToRem(26),
  },
  brand: {
    fontSize: theme.typography.pxToRem(14),
    color: theme.palette.text.secondary,
    fontWeight: "normal",
  },
  barcode: {
    width: "100%",
    margin: "0 auto",
    "& svg": {
      display: "block",
      margin: "0 auto",
      width: "100%",
    },
  },
  button: {
    display: "block",
    width: "70%",
    margin: "0 auto",
  },
}));

const ProductCard: React.FunctionComponent<React.PropsWithChildren<Props>> = (
  props
) => {
  const { item, isEditAble } = props;
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <CardContent className={classes.cardContainer}>
        <img
          alt={item.productName || "Undefined"}
          src={
            item.image ||
            "https://www.eag-led.com/wp-content/uploads/2017/04/Product-Image-Coming-Soon.png"
          }
          className={classes.image}
        />
        <div className={classes.content}>
          <Typography
            align="left"
            variant={"h2"}
            className={clsx(classes.word, classes.title)}
          >
            {item.productName || "Undefined"}
          </Typography>
          <Typography
            align="left"
            variant={"h4"}
            className={clsx(classes.word, classes.brand)}
          >
            {item.brand || "Undefined"}
          </Typography>

          <Divider style={{ margin: "20px 0 20px" }} />
          <div className={classes.barcode}>
            <Barcode
              value={item.UPC12 + "" || "Undefined"}
              width={1.5}
              height={35}
            />
          </div>

          {isEditAble && (
            <>
              <Divider style={{ margin: "10px 0 30px" }} />
              <MyLink
                href={"/product/edit/" + item.id}
                as={"/product/edit/" + item.id}
                underline={"none"}
              >
                <Button variant="contained" className={classes.button}>
                  Edit
                </Button>
              </MyLink>
            </>
          )}
        </div>
      </CardContent>
    </Card>
  );
};

export default ProductCard;
