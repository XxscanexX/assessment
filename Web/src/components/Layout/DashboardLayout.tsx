import * as React from "react";
import { Theme } from "@mui/material/styles";

import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";

import makeStyles from "@mui/styles/makeStyles";
import Topbar from "../Nav/Topbar";
import Footer from "../Nav/Footer";
import clsx from "clsx";

type Props = {
  title?: string;
  textAlign?: any;
  children?: React.ReactNode;
};

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    paddingTop: 56,
    height: "100%",
    [theme.breakpoints.up("sm")]: {
      paddingTop: 64,
    },
  },
  content: {
    padding: theme.spacing(4),
    minHeight: "100%",
  },
  title: {
    marginBottom: theme.spacing(2),
    [theme.breakpoints.down("lg")]: {
      textAlign: "center",
    },
  },
}));

const DashboardLayout: React.FunctionComponent<
  React.PropsWithChildren<Props>
> = ({ children, title, textAlign }) => {
  const classes = useStyles();

  return (
    <div
      className={clsx({
        [classes.root]: true,
      })}
    >
      <div style={{ position: "relative" }}>
        <Box sx={{ display: "flex" }}>
          <Topbar />
        </Box>
        <main className={classes.content}>
          {title ? (
            <Typography
              variant="h1"
              className={classes.title}
              align={textAlign}
            >
              {title}
            </Typography>
          ) : undefined}
          {children}
        </main>
        <Footer />
      </div>
    </div>
  );
};

export default DashboardLayout;
