const MuiTypography = {
  styleOverrides: {
    gutterBottom: {
      marginBottom: 8,
    },
  },
};

export default MuiTypography;
