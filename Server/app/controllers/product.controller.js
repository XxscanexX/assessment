const { sequelize } = require("../../models");
const db = require("../../models");

const Product = db.products;
const Op = db.Sequelize.Op;

// Create and Save a new Product
exports.create = (req, res) => {
  // Validate request
  if (
    !req.body.brand ||
    !req.body.productName ||
    !req.body.UPC12 ||
    !/^\d{12}$/.test(req.body.UPC12 + "") || // Test with regex for valid number only
    (req.body.image &&
      !/(http)?s?:?(\/\/[^"']*\.(?:png|jpg|jpeg|gif|png|svg))/.test(
        req.body.image + ""
      )) // Test with url for valid image only
  ) {
    res.status(400).send({
      message: "Invalid Input!",
    });
    return;
  }

  // Create a Product
  const product = {
    brand: req.body.brand,
    productName: req.body.productName,
    UPC12: req.body.UPC12,
    image: req.body.image || "",
  };

  // Save Product in the database
  Product.create(product)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the product.",
      });
    });
};

// Retrieve all Products from the database.
exports.findAll = (req, res) => {
  const search = req.query.search || "";
  const sort = req.query.sort ? parseInt(req.query.sort) : 0;

  let orderField = null;
  let orderType = null;

  switch (sort) {
    case 1:
      orderField = "productName";
      orderType = "ASC";
      break;
    case 2:
      orderField = "productName";
      orderType = "DESC";
      break;
    case 3:
      orderField = "brand";
      orderType = "ASC";
      break;
    case 4:
      orderField = "brand";
      orderType = "DESC";
      break;
    default:
      orderField = null;
      orderType = null;
      break;
  }

  let condition = search
    ? {
        [Op.or]: [
          { brand: { [Op.like]: `%${search}%` } },
          { productName: { [Op.like]: `%${search}%` } },
        ],
      }
    : null;

  Product.findAll({
    where: condition,
    order: orderField && orderType ? [[orderField, orderType]] : null,
  })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving products.",
      });
    });
};

// Find a single Product with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Product.findByPk(id)
    .then((data) => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({
          message: `Cannot find the product.`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Error retrieving product",
      });
    });
};

// Update a Product by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  // Validate request
  if (
    !req.body.brand ||
    !req.body.productName ||
    !req.body.UPC12 ||
    !/^\d{12}$/.test(req.body.UPC12 + "") || // Test with regex for valid number only
    (req.body.image &&
      !/^((?:https?:\/\/)?(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b[-a-zA-Z0-9@:%_\+.~#?&\/=]*|)$/.test(
        req.body.image + ""
      )) // Test with url for valid image only
  ) {
    res.status(400).send({
      message: "Invalid Input!",
    });
    return;
  }

  Product.update(
    {
      brand: req.body.brand,
      productName: req.body.productName,
      UPC12: req.body.UPC12,
      image: req.body.image || "",
      updatedAt: sequelize.fn("NOW"),
    },
    {
      where: { id: id },
    }
  )
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Product was updated successfully.",
        });
      } else {
        res.send({
          message: `Cannot update product. Product not found or fields update are incorrect!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Error updating product.",
      });
    });
};

// Delete a product with specified ids in the request
exports.deleteMany = (req, res) => {
  const ids = req.params.ids;

  if (Array.isArray(ids) && !ids.every((id) => typeof id === "string")) {
    res.status(400).send({
      message: "Invalid Input!",
    });
    return;
  }

  Product.destroy({
    where: { id: { [Op.in]: ids } },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Product was deleted successfully!",
        });
      } else {
        res.send({
          message: `Cannot delete product. Product not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Could not delete product",
      });
    });
};
