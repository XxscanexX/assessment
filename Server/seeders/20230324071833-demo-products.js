"use strict";

const { v4: uuid } = require("uuid");

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "products",
      [
        {
          id: uuid(),
          brand: "Apple Lady",
          productName: "Apple",
          UPC12: 123456789456,
          image:
            "https://images.unsplash.com/photo-1568702846914-96b305d2aaeb?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
        },
        {
          id: uuid(),
          brand: "Sunset",
          productName: "Orange",
          UPC12: 123456789457,
          image:
            "https://images.unsplash.com/photo-1582979512210-99b6a53386f9?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
        },
        {
          id: uuid(),
          brand: "Coca-Cola",
          productName: "Coca-Cola",
          UPC12: 123456789458,
          image:
            "https://images.unsplash.com/photo-1554866585-cd94860890b7?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=765&q=80",
        },
        {
          id: uuid(),
          brand: "Pepsi",
          productName: "Pepsi",
          UPC12: 123456789459,
          image:
            "https://images.unsplash.com/photo-1629203851122-3726ecdf080e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1529&q=80",
        },
        {
          id: uuid(),
          brand: "Cu Co",
          productName: "Cucumber",
          UPC12: 123456789460,
          image:
            "https://images.unsplash.com/photo-1449300079323-02e209d9d3a6?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1074&q=80",
        },
        {
          id: uuid(),
          brand: "Welt-Holzer",
          productName: "Welt-Holzer matches",
          UPC12: 123456789461,
          image:
            "https://images.unsplash.com/photo-1629573007697-99c6bd69362b?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1440&q=80",
        },
        {
          id: uuid(),
          brand: "Spritzer",
          productName: "Mineral Water",
          UPC12: 123456789461,
        },
        {
          id: uuid(),
          brand: "Sugar Crush",
          productName: "Lollipop",
          UPC12: 123456789461,
          image:
            "https://images.unsplash.com/photo-1575224300306-1b8da36134ec?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
        },
        {
          id: uuid(),
          brand: "Bear Brick",
          productName: "Gummy Bear",
          UPC12: 123456789461,
          image:
            "https://images.unsplash.com/photo-1582058091505-f87a2e55a40f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
        },
        {
          id: uuid(),
          brand: "M&M",
          productName: "M&M",
          UPC12: 123456789461,
          image:
            "https://images.unsplash.com/photo-1534705867302-2a41394d2a3b?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
        },
        {
          id: uuid(),
          brand: "Apple Lady",
          productName: "Pineapple",
          UPC12: 123456789456,
          image:
            "https://images.unsplash.com/photo-1490885578174-acda8905c2c6?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1469&q=80",
        },
        {
          id: uuid(),
          brand: "Zfarm",
          productName: "Watermelon",
          UPC12: 123456789457,
          image:
            "https://images.unsplash.com/photo-1587049352846-4a222e784d38?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
        },
        {
          id: uuid(),
          brand: "Bourbon",
          productName: "Biscuit",
          UPC12: 123456789458,
          image:
            "https://images.unsplash.com/photo-1603194556500-bdd4c947a952?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
        },
        {
          id: uuid(),
          brand: "Hershey",
          productName: "Hershey Chocolate Bar",
          UPC12: 123456789459,
          image:
            "https://images.unsplash.com/photo-1600231409360-e58ed635543a?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=886&q=80",
        },
        {
          id: uuid(),
          brand: "Baskin Robbins",
          productName: "Ice cream cone",
          UPC12: 123456789460,
          image:
            "https://images.unsplash.com/photo-1591325441738-bb3260efbc57?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
        },
        {
          id: uuid(),
          brand: "OilM",
          productName: "Massage Oil (Lavendar)",
          UPC12: 123456789461,
          image:
            "https://images.unsplash.com/photo-1600493570893-3d5c65dce3c5?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
        },
        {
          id: uuid(),
          brand: "HL",
          productName: "Full cream Milk",
          UPC12: 123456789461,
        },
        {
          id: uuid(),
          brand: "Nescafe",
          productName: "Nescafe Gold",
          UPC12: 123456789461,
          image:
            "https://images.unsplash.com/photo-1553052484-d8525f4e7fb6?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=765&q=80",
        },
        {
          id: uuid(),
          brand: "RedBull",
          productName: "RedBull Can",
          UPC12: 123456789461,
          image:
            "https://images.unsplash.com/photo-1613218222876-954978a4404e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
        },
        {
          id: uuid(),
          brand: "Zfarm",
          productName: "Onion",
          UPC12: 123456789461,
          image:
            "https://images.unsplash.com/photo-1587049633312-d628ae50a8ae?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("products", null, {});
  },
};
