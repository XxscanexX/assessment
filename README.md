## Getting Started

Setup MySQL Server with the migration and seed provided. (Under server folder)

Install all the necessary package using command below for both folder

```bash
npm install
# or
yarn install
```

## Start the express server (server folder)

Run Express Server for the API services:

```bash
npm run dev
# or
yarn dev
```

## Start the Next JS server for Front End (web folder)

Run Next js for the Front End (React):

```bash
npm run dev
# or
yarn dev
```

## Screenshot fo the pages

Image 1 - Index Page:
![index page](/screenshot/index.png)

Image 2 - Index Page (Search):
![index page - search](/screenshot/index-search.png)

Image 3 - Index Page (Sort):
![index page - sort](/screenshot/index-sort.png)

Image 4 - Edit Product:
![edit page](/screenshot/edit.png)

Image 5 - Edit Product (Error):
![edit page - error](/screenshot/edit-error.png)
